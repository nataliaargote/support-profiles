package co.moviired.support.helper;
/*
 * Copyright @2019. Movii, SAS. Todos los derechos reservados.
 *
 * @author Bejarano, Cindy
 * @version 1, 2019
 * @since 1.0
 */

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public Utils(){
        super();
    }

    public static final String generateCorrelationId() {
        String result = java.util.UUID.randomUUID().toString();

        result=result.replaceAll("-", "");
        result=result.substring(0, 32);
        return result;
    }

}
