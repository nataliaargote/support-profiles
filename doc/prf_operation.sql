/*
-- Query: SELECT * FROM movii_nuevos_convenios.prf_operation
LIMIT 0, 1000

-- Date: 2019-10-03 13:56
*/
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (1,'TOP_UP','Recargas celulares ','/topup',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (2,'GIROS','Giros','/giros',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (3,'RETIROS','Retiros','/retiros',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (4,'DEPOSITOS','Depositos','/depositos',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (5,'IMAGES','Banner Home','/images',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (6,'CONSIGNMENTS','Consignaciones','/consignments',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (7,'PROFILES',' Perfiles activos','/moviired-api/support-profiles/v1/profiles/status/1',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (8,'AUTHORIZATION','Autorizacion y refresh_token','/moviired-api/authentication/v1/oauth/token',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (9,'CHANGEPASSWORD','ChangePass','/moviired-api/authentication/v1/changePassword',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (10,'RESETPASSWORD','ResetPass','/moviired-api/authentication/api/resetPassword',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (11,'GENERATEOTP','GenerateOtp','/moviired-api/authentication/api/generateOTP',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (12,'GETPROFILEIMAGE','Obtener profile type image','/moviired-api/supportgeneral/v1/images/[A-Z]{1,10}/[A-Z]{1,7}',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (13,'GETALLIMAGES','Obtener todas las imagenes','/moviired-api/supportgeneral/v1/images/findAll',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (14,'MODIFICARIMAGENES','Modificar las imagenes','/moviired-api/supportgeneral/v1/images/modify',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (15,'CARGARIMAGEN','Cargar Imagen','/moviired-api/supportgeneral/v1/images/upload',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (16,'CONSIGNMENTSGETSALDO','Obtener Saldo Consignaciones','/moviired-api/srv-cash/v1/consignments/user/balance',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (17,'CONSIGNMENTSGETFILTER','Filtrar consignaciones','/moviired-api/srv-cash/v1/consignments/list/filter',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (18,'CONSIGNMENTSGETBANK','Obtener bancos','/moviired-api/srv-cash/v1/consignments/bank/list',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (19,'CONSIGNMENTSREGISTRY','Registro consignacion','/moviired-api/srv-cash/v1/consignments/user/registry',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (20,'CONSIGNMENTSAPPROVE','Aprobar consignaciones','/moviired-api/srv-cash/v1/consignments/approve',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (21,'CONSIGNMENTSREJECT','Rechazar consignaciones','/moviired-api/srv-cash/v1/consignments/reject',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (22,'CONSIGNMENTSGETVOUCHER','Obtener voucher consignación','/moviired-api/srv-cash/v1/consignments/voucher/[0-9]{1,4}',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (23,'GETUSER','Obtener Usuario','/moviired-api/supportusers/v1/findAllUsers',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (24,'REGISTERUSER','Registrar Usuario','/moviired-api/supportusers/v1/createUser',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (25,'MODIFYUSER','Modificar','/moviired-api/supportusers/v1/updateUser',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (32,'TOPUPBUSINESS','Topup Businness','/movii-api/operators/v1/[0-9]{1,4}',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (33,'CONSIGNMENTSUPDATER','Actualizar consignacion','/moviired-api/srv-cash/v1/consignments/user/update',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (34,'SETPASSWORDMAINDRA','Cambiar el mpin ','/moviired-api/supportusers/v1/setPasswordMahindra',1);
INSERT INTO `` (`id`,`name`,`operation_description`,`operation_url`,`status`) VALUES (35,'RESENDOTP','ResendOtp','/moviired-api/authentication/api/resendOTP',1);
